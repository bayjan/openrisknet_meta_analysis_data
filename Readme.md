# Data for the meta-analysis project as a case study for OpenRiskNet project

This repository contains only data for the meta-analysis project as a case study for OpenRiskNet project. Only meta-data information file,  normalized data and average of normalized array data of all replicates (mostly 3 replicates) are available in the sub-folders.
Please also read the Readme.md files present in the subfolders.