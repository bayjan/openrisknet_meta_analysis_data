# Average of normalized data

The folder **average_ratio** contains average of normalized array data of all replicates (mostly 3 replicates) present in the folder **normalized_data** using the information present in the meta-data information file located in **meta_data** folder.
